const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const calltypeRoutes = require('./api/routes/calltypes');
const privacypolicyRoutes = require('./api/routes/privacypolicies');
const termsofuseRoutes = require('./api/routes/termsofuses');
const phonelogRoutes = require('./api/routes/phonelogs');
const userRoutes = require('./api/routes/users');

mongoose.connect('mongodb://localhost:27017/phonelog-api', { useNewUrlParser: true, useUnifiedTopology: true } );
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Connection Successful!");
});



app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.use((req, res, next)=> {
    res.header("Access-Control-Allow-origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );

    if(req.method ==='OPTIONS'){
    res.header("Access-Control-Allow-Methods", 'PUT, POST, GET, PATCH, DELETE')
    return res.status(200).json({});
}
next();
});

app.use('/PhoneLog-Api/calltypes', calltypeRoutes);
app.use('/PhoneLog-Api/privacypolicies', privacypolicyRoutes);
app.use('/PhoneLog-Api/termsofuses', termsofuseRoutes);
app.use('/PhoneLog-Api/phonelogs', phonelogRoutes);
app.use('/PhoneLog-Api/users', userRoutes);

app.use((req, res, next)=>{
    const error = new Error('Not found anything');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error:{
            message : error.message
        }
    })
})

db.close();

module.exports = app;