const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    name : { 
        type : String , 
        required : true, 
        minlength : 3
    },
    defaultUser : {
        type :  String,
        required : true,
    },
    activationToken : String,
    users : [{
        id : String,
        name : {
            type : String,
            required : true,
            trim : true,
            minlength : 3
        },
        surname : {
            type : String,
            required : true,
            trim : true
        },
        isGAL : Boolean,
        emails : Array,
        userEmail : {
            type : String,
            required : true,
            trim : true
        },
        default : Boolean,
    }],
    status : String,
    acceptedPP : Array,
    acceptedToU : Array,
    defaultBrand : String,
    showGlobalAddressList : Boolean,
    archivePhoneLogs : Boolean,
    timeFormat24Hour : Boolean,
    pushNotifications : Boolean,
    refreshInterval : Number,
    showProfilePhoto : Boolean,
    devices :  Array,
    updatedAt : Date,
    createdAt : Date,
});

module.exports = mongoose.model('User', UserSchema);