const mongoose = require('mongoose');

const TermsOfUseSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    content : { type : String, required:true },
    version : { type :String, required:true },
    date : { type : Date , required : true },
    updatedAt : { type:  Date , required:true },   
    createdAt : { type : Date, required:true }
});

module.exports = mongoose.model('TermsOfUse', TermsOfUseSchema)
