const mongoose = require('mongoose');
const validator = require('validator');

const CallTypeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    createdBy : {
        id : mongoose.Schema.Types.ObjectId,
        name : { 
            type : String , 
            required : true,
            trim : true,
            minlength : 3
        },
        email : {
            type : String , 
            required : true,
            trim : true,
            validate(value){
                if(!validator.isEmail(value)){
                    throw new Error('Email is invalid');
                }
            }
        }
    },
    category : { type : String , required : true},
    icon : { type : String , required : true},
    name : { 
        type : String , 
        required : true,
        trim : true
    },
    deleted : Boolean,
    updatedAt : { type : Date, required : true},
    createdAt : { type : Date, required: true},
});

module.exports = mongoose.model('CallType', CallTypeSchema)
