const mongoose  = require('mongoose');

const PhoneLogSchema = mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    userId : {
        type : String,
        required : true,
        trim : true
    },
    createdBy : mongoose.Schema.Types.ObjectId,
    contact : {
        id : String,
        name : {
            type : String,
            required : true,
            trim : true,
            minlength : 3
        },
        surname : {
            type : String,
            required : true,
            trim : true
        },
        company : {
            type : String,
            required : true,
            trim : true
        },
        isGAl : false,
        emails : Array,
        phoneNumbers : Array,
        addresses : Array,
    },
    phoneNumber : {
        id : String,
        type : { type :String, required :true },
        phoneNumber : { type :String, required :true }
    },
    datetime : Date,
    callType : String,
    note : String,
    urgent : Boolean,
    sendNotification : Boolean,
    archived : Boolean,
    deleted : Boolean,
    previousStates : Array,
    readers : Array,
    updatedAt : Date,
    createdAt : Date,
});

module.exports = mongoose.model('PhoneLog', PhoneLogSchema);