const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const PhoneLog = require('../models/PhoneLog');

router.get('/', (req, res, next) => {
    PhoneLog.find()
        .exec()
        .then(phonelog =>{
            console.log(phonelog); 
            res.status(200).json(phonelog);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error})
        })
});

router.post('/', (req, res, next) => {
    const phonelog = new PhoneLog({
        _id: new mongoose.Types.ObjectId(),
        userId : req.body.userId,
        createdBy : new mongoose.Types.ObjectId(),
        contact : {
            id : req.body.contact.id,
            name : req.body.contact.name,
            surname : req.body.contact.surname,
            company : req.body.contact.company,
            isGAL : req.body.contact.isGAL,
            emails : req.body.contact.emails,
            phoneNumbers : req.body.contact.phoneNumbers,
            addresses : req.body.contact.addresses
        },
        phoneNumber : {
            id : req.body.phoneNumber.id,
            type : req.body.phoneNumber.type,
            phoneNumber : req.body.phoneNumber.phoneNumber,
        },
        datetime : req.body.datetime,
        callType : req.body.callType,
        note : req.body.note,
        urgent : req.body.urgent,
        sendNotification : req.body.sendNotification,
        archived : req.body.archived,
        deleted : req.body.deleted,
        previousStates : req.body.previousStates,
        readers : req.body.readers,
        updatedAt : req.body.updatedAt,
        createdAt : req.body.createdAt
    })
    phonelog.save()
    .then(result =>{
        console.log(result);
    })
    res.status(200).json({
        message :'post',
        createdUser : phonelog
    })
});

router.get("/:phonelogId", (req, res, next) => {
    const id = req.params.phonelogId;
    PhoneLog.findById(id)
        .exec()
        .then(phonelog =>{
            console.log(phonelog);
            if(calltype){
                res.status(200).json(phonelog);
            }else {
                res.status(404).json({message :"NO Valid Entry Found"});
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error:error});
        })
});

router.delete('/:phonelogId', (req, res, next) => {
    const id = req.params.phonelogId;
    PhoneLog.remove({_id :id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error});
        })
});

module.exports = router;
