const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const CallType = require('../models/CallType');

router.get('/', (req, res, next) => {
    CallType.find()
        .exec()
        .then(calltype =>{
            console.log(calltype); 
            res.status(200).json(calltype);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error})
        })
});

router.post('/', (req, res, next) => {
    const calltype = new CallType({
        _id: new mongoose.Types.ObjectId(),
        createdBy : {
            id : new mongoose.Types.ObjectId(),
            name : req.body.createdBy.name,
            email : req.body.createdBy.email
        },
        category : req.body.category,
        icon : req.body.icon,
        name : req.body.name,
        deleted : req.body.deleted,
        updatedAt : req.body.updatedAt,
        createdAt : req.body.createdAt
    })
    calltype.save()
    .then(result =>{
        console.log(result);
    })
    res.status(200).json({
        message :'post',
        createdUser : calltype
    })
});

router.get("/:calltypeId", (req, res, next) => {
    const id = req.params.calltypeId;
    CallType.findById(id)
        .exec()
        .then(calltype =>{
            console.log(calltype);
            if(calltype){
                res.status(200).json(calltype);
            }else {
                res.status(404).json({message :"NO Valid Entry Found"});
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error:error});
        })
});

router.patch('/:calltypeId', (req, res, next) => {
   const id = req.params.calltypeId;
   const updateOps = {};
   for(const ops of req.body){
       updateOps[ops.propName] = ops.value;
   }
   CallType.update({_id :id}, { $set:updateOps})
       .exec()
       .then(result => {
           console.log(result);
           res.status(200).json(result);
       })
       .catch(error =>{
           console.log(error);
           res.status(404).json({error:error});
       })
});

router.delete('/:calltypeId', (req, res, next) => {
    const id = req.params.calltypeId;
    CallType.remove({_id :id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error});
        })
});


module.exports = router;
