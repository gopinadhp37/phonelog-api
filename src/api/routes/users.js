const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('../models/User');

router.get('/', (req, res, next) => {
    User.find()
        .exec()
        .then(user =>{
            console.log(user); 
            res.status(200).json(user);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error})
        })
});

router.post('/', (req, res, next) => {
    const user = new User({
        _id :new mongoose.Types.ObjectId(),
        name : req.body.name,
        defaultUser : req.body.defaultUser,
        activationToken : req.body.activationToken,
        users : [{
            id : req.body.users[0].id,
            name : req.body.users[0].name,
            surname : req.body.users[0].surname,
            isGAL : req.body.users[0].isGAL,
            emails : req.body.users[0].emails,
            userEmail : req.body.users[0].userEmail,
            default : req.body.users[0].default,
        }],
        status : req.body.status,
        acceptedPP : req.body.acceptedPP,
        acceptedToU : req.body.acceptedToU,
        defaultBrand : req.body.defaultBrand,
        showGlobalAddressList : req.body.showGlobalAddressListy,
        archivePhoneLogs : req.body.archivePhoneLogs,
        timeFormat24Hour : req.body.timeFormat24Hour,
        pushNotifications : req.body.pushNotifications,
        refreshInterval : req.body.refreshInterval,
        showProfilePhoto : req.body.showProfilePhoto,
        devices :  req.body.devices,
        updatedAt : req.body.updatedAt,
        createdAt : req.body.createdAt
    })
    user.save()
    .then(result =>{
        console.log(result);
    })
    res.status(200).json({
        message :'post',
        createdUser : user
    })
});

router.get("/:userId", (req, res, next) => {
    const id = req.params.userId;
    User.findById(id)
        .exec()
        .then(user =>{
            console.log(user);
            if(user){
                res.status(200).json(user);
            }else {
                res.status(404).json({message :"NO Valid Entry Found"});
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error:error});
        })
});

router.delete('/:userId', (req, res, next) => {
    const id = req.params.userId;
    User.remove({_id :id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error});
        })
});

module.exports = router;
