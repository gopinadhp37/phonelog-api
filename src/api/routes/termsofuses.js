const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const TermsOfUse = require('../models/TermsOfUse');

router.get('/', (req, res, next) => {
    TermsOfUse.find()
        .exec()
        .then(termsofuse =>{
            console.log(termsofuse); 
            res.status(200).json(termsofuse);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error})
        })
});

router.post('/', (req, res, next) => {
    const termsofuse = new TermsOfUse({
        _id: new mongoose.Types.ObjectId(),
        content : req.body.content,
        version : req.body.version,
        date : req.body.date,
        updatedAt : req.body.updatedAt,
        createdAt : req.body.createdAt
    })
    termsofuse.save()
    .then(result =>{
        console.log(result);
    })
    res.status(200).json({
        message :'post',
        createdUser : termsofuse
    })
});

router.get("/:termsofuseId", (req, res, next) => {
    const id = req.params.termsofuseId;
    PrivacyPolicy.findById(id)
        .exec()
        .then(termsofuse =>{
            console.log(termsofuse);
            if(termsofuse){
                res.status(200).json(termsofuse);
            }else {
                res.status(404).json({message :"NO Valid Entry Found"});
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error:error});
        })
});

router.patch('/:termsofuseId', (req, res, next) => {
   const id = req.params.termsofuseId;
   const updateOps = {};
   for(const ops of req.body){
       updateOps[ops.propName] = ops.value;
   }
   TermsOfUse.update({_id :id}, { $set:updateOps})
       .exec()
       .then(result => {
           console.log(result);
           res.status(200).json(result);
       })
       .catch(error =>{
           console.log(error);
           res.status(404).json({error:error});
       })
});

router.delete('/:termsofuseId', (req, res, next) => {
    const id = req.params.termsofuseId;
    TermsOfUse.remove({_id :id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error});
        })
});


module.exports = router;
