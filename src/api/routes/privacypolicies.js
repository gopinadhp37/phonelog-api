const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const PrivacyPolicy = require('../models/PrivacyPolicy');

router.get('/', (req, res, next) => {
    PrivacyPolicy.find()
        .exec()
        .then(privacypolicy =>{
            console.log(privacypolicy); 
            res.status(200).json(privacypolicy);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error})
        })
});

router.post('/', (req, res, next) => {
    const privacypolicy = new PrivacyPolicy({
        _id: new mongoose.Types.ObjectId(),
        content : req.body.content,
        version : req.body.version,
        date : req.body.date,
        updatedAt : req.body.updatedAt,
        createdAt : req.body.createdAt
    })
    privacypolicy.save()
    .then(result =>{
        console.log(result);
    })
    res.status(200).json({
        message :'post',
        createdUser : privacypolicy
    })
});

router.get("/:privacypolicyId", (req, res, next) => {
    const id = req.params.privacypolicyId;
    PrivacyPolicy.findById(id)
        .exec()
        .then(privacypolicy =>{
            console.log(privacypolicy);
            if(privacypolicy){
                res.status(200).json(privacypolicy);
            }else {
                res.status(404).json({message :"NO Valid Entry Found"});
            }
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({error:error});
        })
});

router.patch('/:privacypolicyId', (req, res, next) => {
   const id = req.params.privacypolicyId;
   const updateOps = {};
   for(const ops of req.body){
       updateOps[ops.propName] = ops.value;
   }
   PrivacyPolicy.update({_id :id}, { $set:updateOps})
       .exec()
       .then(result => {
           console.log(result);
           res.status(200).json(result);
       })
       .catch(error =>{
           console.log(error);
           res.status(404).json({error:error});
       })
});

router.delete('/:privacypolicyId', (req, res, next) => {
    const id = req.params.privacypolicyId;
    PrivacyPolicy.remove({_id :id})
        .exec()
        .then(result => {
            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({ error:error});
        })
});


module.exports = router;
